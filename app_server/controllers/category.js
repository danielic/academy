var request = require('request');
var { apiCategories } = require('./constants');

//Get home page
var renderHomepage = function (req, res, body) {
    res.render('layout',{
        title: 'Zara',
        category: body
    });
}

//Render homepage after the response from apiCategorries is receievd
module.exports.homepage= function (req, res) {
    var requestOptions;
    requestOptions =
    {
        url: apiCategories,
        method: "GET",
        json: {} 
    };
    request(requestOptions, function(err, response, body) {
        renderHomepage(req,res,body);
    });
}

//Get category page
var renderCategoryPage = function(req, res,category){
    res.render('layout-category',{
        title: 'Zara',
        category: category,
    });
}
//Render categories after the response from apiCategorries is receievd
module.exports.categories = function(req,res){
    var requestOptions;
    requestOptions =
    {
        url : apiCategories + req.params.categoryid ,
        method : "GET" ,
        json:{}
    };
   request(requestOptions, function (err, response, category) {
       renderCategoryPage(req,res,category);
   })
}

//Get subcategory page
var renderSubcategoryPage = function(req, res, subcategory) {
    res.render('layout-subcategory',{
        title: 'Zara',
        subcategory: subcategory,
        categoryid: req.params.categoryid,
        subcategoryid: req.params.subcategoryid,
    });
}
//Render subcategories after the response from apiCategorries is receievd
module.exports.subcategories = function(req, res) {
    var requestOptions;
    requestOptions =
    {
        url: apiCategories + req.params.categoryid + "/" + req.params.subcategoryid,
        method:"GET",
        json:{}
    };
    request(requestOptions, function(err, response, subcategory) {
        renderSubcategoryPage(req,res,subcategory);
    });
}

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};
