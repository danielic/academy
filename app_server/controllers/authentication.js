var request = require('request');
var { apiSignUp, apiSignIn, apiSecret ,apiActivate } = require('./constants');
//Get Register Page
var renderRegister = function (req, res) {
    res.render('layout-register',{
        title : 'Zara'
    }); 
}
module.exports.renderRegisterPage = function(req, res) {
    renderRegister(req,res);  
};
//Post register Page
module.exports.register = function (req, res) {
    var requestOptions, postdata;

    postdata = {
       email : req.body.email ,
       username : req.body.username,
       password : req.body.password,
       confirmationPassword : req.body.confirmationPassword
    }
    requestOptions = {
        url : apiSignUp,
        method : "POST",
        json : postdata
    }
    if(!postdata.email || !postdata.username || !postdata.password) {
        res.redirect('/authenticate/signup?err=val');
    }else{
        request(requestOptions, function (err, response) {
            if(response.statusCode === 201)
            {  
                res.redirect('/authenticate/signup?msg='+ response.body.success);
            }else if(response.statusCode === 400 && response.body.name && response.body.name === "ValidationError"){
                res.redirect('/authenticate/signup?msg='+response.body.name);
            }else if(response.statusCode === 403 ){
                res.redirect('/authenticate/signup?msg='+ response.body.error);
            }
        })
    }
}

//Get Register Page
var renderLogIn = function(req,res) {
    res.render('layout-login',{
        title : 'Zara'
    });
    
}
module.exports.renderLogInPage = function(req, res) {
    renderLogIn(req,res);  
};

//Post Log In user
module.exports.login = function (req, res) {
    var requestOptions, postdata;

    postdata = {
        email : req.body.email,
        password : req.body.password
    }

    requestOptions = {
        url : apiSignIn,
        method : "POST",
        json : postdata
    }
  
    if(!postdata.email || !postdata.password){
        res.redirect('/authenticate/login?err=val')
    }else{
        request(requestOptions, function (err, response){
              if(response.statusCode === 200){
                  res.cookie('Authorization',response.body.token);
                  res.redirect('/authenticate/login');
              }else{
                res.redirect('/authenticate/login?msg=NotAuthorized');  
              }
            })     
        }
}
//Render Secret Page
var renderSecret = function (req,res, body) {
    res.render('layout-secret',{
        title : 'Zara',
        secret : body
    });
    
}
//Secret controller
module.exports.secret = function (req, res, body){
    var requestOptions;

    requestOptions = {
        url : apiSecret,
        method : "GET",
        headers:{
            'Authorization' : req.cookies.Authorization
        },
        json: {}
    }
    request(requestOptions, function(err, response, body) {
        renderSecret(req,res,body);
    });
};


var renderActivate = function(req,res){
    var requestOptions;
    requestOptions = {
        url : apiActivate + req.params.token,
        method : "POST",
        json: {}
    }
  
    if(!req.params.token){
        res.render('layout-activate',{
            title : 'Zara',
            token : 'This is not the link you are looking for!'
        });
    }else{
        request(requestOptions, function (err, response){
              if(response.statusCode === 201){
                res.redirect('/authenticate/login');
              }
            })     
        }
}
module.exports.renderActivatePage = function(req, res){
    renderActivate(req,res);  
};

module.exports.logout = function (req, res){
     res.clearCookie('Authorization');
     res.redirect('/');
}






