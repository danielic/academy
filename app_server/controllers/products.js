var request = require('request');
var { apiProducts } = require('./constants');
var { apiCurrency } = require('./constants');
//Get Products page
var renderProductsPage = function(req, res, body) {
    res.render('layout-products',{
        title: 'Zara',
        products: body,
        categoryid: req.params.categoryid,
        subcategoryid: req.params.subcategoryid,
        product: req.params.products
    });
}
//Render products after the response from apiProducts is receievd
module.exports.Products= function(req, res) {
    var requestOptions;
    requestOptions =
    {
        url: apiProducts + req.params.products,
        method: "GET",
        json: {} 
    };
    request(requestOptions, function(err, response, body) {
        renderProductsPage(req,res,body);
    });

}
//Get Product page
var renderProductPage = function (req, res, body,currency) {
    res.render('layout-product',{
        title: 'Zara',
        product: body,
        categoryid: req.params.categoryid,
        subcategoryid: req.params.subcategoryid,
        products: req.params.products,
        productid: req.params.productid,
        currency: currency
    });
}
//Render one product after the response from apiProducts is receievd
//After the product data is received call the apiCurrency link to get currency data
module.exports.Product = function (req, res) {
    var requestOptions;
    requestOptions =
    {
        url: apiProducts + req.params.products + "/" + req.params.productid,
        method: "GET",
        json: {}
    };
    requestCurrency = {
        url : apiCurrency,
        method: "GET",
        json : {}
    }
    req
    request(requestOptions, function(err, response, body) {
        request(requestCurrency, function (err, response, currency){
            renderProductPage(req, res, body, currency);
        })
    });
}



