var config = require('../../env.json')[process.env.NODE_ENV || 'development'];

module.exports = {
    apiProducts : config.apiProducts,
    apiCategories : config.apiCategories,
    apiCurrency : config.apiCurrency,
    apiSignUp : config.apiSignUp,
    apiSignIn : config.apiSignIn,
    apiActivate : config.apiActivate,
    apiSecret : config.apiSecret,
    apiSearch : config.apiSearch
}