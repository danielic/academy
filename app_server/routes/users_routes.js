var ctrlAuthentication = require('../controllers/authentication');

module.exports = function(router){
    //Get SignUp Page
    router.get('/signup',ctrlAuthentication.renderRegisterPage);
    //Post SignUp user
    router.post('/signup',ctrlAuthentication.register);
    //Get LogIn Page
    router.get('/login',ctrlAuthentication.renderLogInPage);
    //Post Login
    router.post('/login',ctrlAuthentication.login);
    //Get secret page
    router.get('/secret',ctrlAuthentication.secret);
    //Get Activate user route
    router.get('/activate/:token',ctrlAuthentication.renderActivatePage);
    //LogOut Page
    router.get('/logout',ctrlAuthentication.logout);
}