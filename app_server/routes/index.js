var express = require('express');
var router = express.Router();
require('./categories_routes')(router);
require('./products_routes')(router);
module.exports = router;
