var ctrlProducts = require('../controllers/products');

module.exports = function(router){
    //Get products page
    router.get('/:categoryid/subcategory/:subcategoryid/:products',ctrlProducts.Products);
    //Get product page
    router.get('/:categoryid/subcategory/:subcategoryid/:products/:productid',ctrlProducts.Product);
}