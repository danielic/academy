var ctrlCategory = require('../controllers/category');

module.exports = function(router){
    //Get home page
    router.get('/',ctrlCategory.homepage);
    //Get category page
    router.get('/:categoryid',ctrlCategory.categories);
    //Get subcategory page
    router.get('/:categoryid/subcategory/:subcategoryid',ctrlCategory.subcategories);
}