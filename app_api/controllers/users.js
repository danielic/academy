var User = require('../models/users');
var sendJsonResponse = require('./sendJsonResponse');
var mailer = require('./mailer');
//Post user
module.exports.signUp = function (req, res) {
   
    var  {email, username, createdOn, password} = req.value.body;

    var newUser = new User({
        email : email,
        username : username,
        createdOn : createdOn,
        password : password,
        token : (Math.random().toString(36)+'00000000000000000').slice(2, 10+2)
       
    })
    //First check if user exists
    User.findOne({email: email}, function(err, result) {
        if(result){
            //if user exists do nothing
            sendJsonResponse(res, 403,{error : "Email in use!Please try another Email"} );
        }else{
            //else if user not exists create new user
            User.createUser(newUser, function(err, user){
                if(err){
                    sendJsonResponse(res, 401, { error: "User not created!" });
                }else{
                    var token = User.signToken(user);
                    mailer.transporter.sendMail(mailer.helperOptions(user.email, token), function(err, response){
                    });
                    sendJsonResponse(res, 201, { success: "User Created succesfully!Please Check Email",token :token});
                }
            })
        }
    })
}

//Sign In and return the token
module.exports.signIn = function(req,res){
    var token = User.signToken(req.user);
    sendJsonResponse(res, 200, { token: token ,
        success: "User loged in"});
}
//Secret data
module.exports.secret = function(req,res){
    res.json({secret: "Here is data about your user"});
}
//Activate data
module.exports.activate = function (req,res) {
    User.findOne({token : req.user.token},  function(err, result){
       User.activateUser(result, function (err, response){
           if(err){
            sendJsonResponse(res, 401, { error: "Error!" }); 
           }else{
            sendJsonResponse(res, 201, { success: "User Activated! Please login!"});
           }
       });
    }); 
}

