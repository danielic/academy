var Category = require('../models/categories'); 
var sendJsonResponse = require('./sendJsonResponse');

//Categories Controller GetAll categories
module.exports.categoriesAll = function (req, res) {
    Category
    .find({},{"_id":0})
    .select('name page_description categories.id categories.name categories.page_description categories.image id categories.parent_category_id')
    .exec(function(err, result) {
        sendJsonResponse(res, 200, result);
    })
}

//Categories Controller GetAll one category
module.exports.categoryid = function (req, res) {
    Category
    .findOne({'id' : req.params.categoryid},{"_id" : 0})
    .select()
    .exec(function (err, result){
        sendJsonResponse(res, 200, result);
    })
}

//Categories Controller GetAll one subcategory
module.exports.subcategoryid = function (req, res) {
    Category
    .findOne({'id' : req.params.categoryid }, { "_id" : 0 })
    .select({ categories : { $elemMatch : { id:req.params.subcategoryid }}})
    .exec(function (err, result) {
        sendJsonResponse(res, 200, result);
    })
}