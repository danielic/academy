//Function that sends response as JSON
module.exports = function (res, status, content) {
    res.status(status);
	res.json(content);
}