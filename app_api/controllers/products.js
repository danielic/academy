var Products = require('../models/products'); 
var sendJsonResponse = require('./sendJsonResponse');

//Get products for a subcategory
module.exports.products = function (req, res) {
    Products
    .find({'primary_category_id' : req.params.products },{'_id' : 0})
    .select('short_description name price page_title page_description primary_category_id id image_groups')
    .exec(function (req, result) {
        sendJsonResponse(res, 200, result);
    })
}

//Get one products
module.exports.productid = function (req, res) {
    Products
    .find({'id' : req.params.productid})
    .select('name price id currency long_description image_groups')
    .exec(function (req, result){
        sendJsonResponse(res, 200, result);
    });
}

module.exports.findProduct = function (req,res) {
    var regex = new RegExp(escapeRegex(req.params.name), 'gi');
    Products
    .find({name :regex})
    .select('name price primary_category_id id')
    .exec(function (req, result){
        sendJsonResponse(res, 200 , result);
    })
}

function escapeRegex(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};