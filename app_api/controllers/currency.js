var soap = require ('soap');

var sendJsonResponse = function (res, status, content) {
	res.status(status);
	res.json(content);
}

//Get Currency currencies
module.exports.currency = function (req, res) {
    var wsdlUrl = "http://infovalutar.ro/curs.asmx?wsdl";
    //Create soap client
    soap.createClient(wsdlUrl, function(err, soapClient){
            if(err) throw err;
            //Call to get last date inserted
            soapClient.lastdateinserted(function(err, soapResult){
                if(err) throw err;
                var date= new Date(soapResult.lastdateinsertedResult);
                //Call to get all currencies with last date inserted       
                soapClient.getall({dt : date.toISOString()},function(err,result){
                    if(err) throw err;
                    sendJsonResponse(res,200,result.getallResult.diffgram.DocumentElement.Currency);
            });
        });
    });
}






