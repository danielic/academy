var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//category schema
var categorySchema = new Schema({
    categories: {
    },
    id: String,
    name: String,
    page_description: String,
    page_title: String,
    parent_category_id: String,
   
});

var Category = mongoose.model('categories', categorySchema);

module.exports = Category;