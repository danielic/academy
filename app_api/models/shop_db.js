var mongoose = require('mongoose');
var config = require('../../env.json')[process.env.NODE_ENV || 'development'];

//Connect to db
mongoose.connect(config.db);

require('./categories');
require('./products');
require('./users');

