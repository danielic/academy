var Joi = require('joi');

//signup schema validation
 var userSchema = 
 Joi.object().keys({
    email: Joi.string().email().required(),
    username: Joi.string().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{6,15}$/).required(),
    confirmationPassword: Joi.any().valid(Joi.ref('password')).required()
})
//login schema validation
var logSchema = 
Joi.object().keys({
    email: Joi.string().email().required(),
    password: Joi.string().regex(/^[a-zA-Z0-9]{6,15}$/).required()
});

//validate signUp body
module.exports.validateBody = function(schema) {
    return function(req, res, next) {
        var result = Joi.validate(req.body, userSchema);
        if(result.error){
            return res.status(400).json(result.error);
        };
        if(!req.value){
            req.value = {};
        }
        req.value['body']= result.value;
        next();
    };
};
//Validate log in body
module.exports.validateLogIn = function(schema) {
    return function(req, res, next) {
        var result = Joi.validate(req.body, logSchema);
        if(result.error){
            return res.status(400).json(result.error);
        };
        if(!req.value){
            req.value = {};
        }
        req.value['body']= result.value;
        next();
    };
};
