var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var {ExtractJwt} = require('passport-jwt');
var LocalStrategy = require('passport-local').Strategy;
var User = require('./users');

//strategy for header token
passport.use('header',new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromHeader('authorization'),
    secretOrKey : 'academy'
}, function (payload, done) {
     User.findOne({token :payload.sub }, function(err, user){
        if(!user){
            return done(null, false);
        }
        done(null, user);
    });
    
}));
//strategy for url token
passport.use('url',new JwtStrategy({
    jwtFromRequest: ExtractJwt.fromUrlQueryParameter('token'),
    secretOrKey : 'academy'
}, function (payload, done) {
     User.findOne({token :payload.sub }, function(err, user){
        if(!user){
            return done(null, false);
        }
        done(null, user);
    });
    
}));
//Log in strategy
passport.use(new LocalStrategy({
    usernameField : 'email'
}, function (email, password, done){
    User.findOne( {email} , function(err, user){
        if(err) throw err;
        if(!user){
            return done(null, false, {message: "Unknown User"});
        }
        if(user.active === false ){
            return done(null, false, {message: "Not Active"});
        }
        User.comparePassword(password, user.password, function(err, isMatch){
           if(err) throw err;
           if(isMatch){
               return done(null, user);
           } else {
               return done(null, false, {message: "Invalid password"});
           }
        });
    });
}));