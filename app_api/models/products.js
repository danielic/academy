var mongoose = require('mongoose');
var Schema = mongoose.Schema

//Products schema
var productsSchema = new Schema({
    page_description : String,
    page_title : String,
    name : String ,
    price : Number,
    id: String,
    currency : String,
    primary_category_id : String,
    short_description : String,
    long_description : String
});

var Products = mongoose.model('products', productsSchema);

module.exports = Products;