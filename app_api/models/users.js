var mongoose = require('mongoose');
var bcrypt = require('bcrypt');
var JWT = require ('jsonwebtoken');

var Schema = mongoose.Schema;

var userSchema = new Schema({
    email : String,
    username : String,
    password: String,
    createdOn : {
        type : Date,
        "default" : Date.now
    },
    active: {
        type : Boolean,
        "default" : false
    },
    token: String
})


var User = mongoose.model('users', userSchema);


module.exports = User;

//Create user and hash the password
module.exports.createUser = function (newUser, callback) {
    bcrypt.genSalt(10, function(err, salt){
        bcrypt.hash(newUser.password, salt,function(err, hash){
            newUser.password = hash;
            newUser.save(callback);
        })
    })
}
//Activate user
module.exports.activateUser = function (user, callback) {
        user.active = true
        user.save(callback);
}

//Compare password when logging in
module.exports.comparePassword = function (candidatePassword, hash, callback){
    bcrypt.compare(candidatePassword, hash, function(err, isMatch){
        if(err) throw err;
        callback(null, isMatch);
    })
}
//Sign tokens
module.exports.signToken = function (newUser) {
    return JWT.sign({
        iss: "authorization",
        sub: newUser.token,
        iat: new Date().getTime(),
        exp: new Date().setDate(new Date().getDate() +1)
    }, 'academy');
}



