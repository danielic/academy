var ctrlCategories = require('../controllers/categories');

module.exports = function(router){
    //Get all categories route
    router.get('/categories',ctrlCategories.categoriesAll);
    //Get category route
    router.get('/categories/:categoryid', ctrlCategories.categoryid);
    //Get subcategory route
    router.get('/categories/:categoryid/:subcategoryid', ctrlCategories.subcategoryid);
}