var ctrlCurrency = require('../controllers/currency');

module.exports = function(router){
    //Get currency currencies
    router.get('/currency', ctrlCurrency.currency);
}