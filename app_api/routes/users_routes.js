var usersController = require('../controllers/users');
var validateHelper = require('../models/helper');
var passport = require('passport');
var passportConfig = require('../models/passport');
module.exports = function(router){
    //Post user after is validated
    router.post('/signup',validateHelper.validateBody(validateHelper.userSchema), usersController.signUp);
    //Loging in after the user is validated
    router.post('/signin',validateHelper.validateLogIn(validateHelper.logSchema) ,passport.authenticate('local', {session: false}) ,usersController.signIn);
    //Get data from the secret path
    router.get('/secret',passport.authenticate('header', {session: false}), usersController.secret);
    //Activate user route
    router.post('/activate',passport.authenticate('url', {session : false}), usersController.activate);
}