var ctrlProducts = require('../controllers/products');

module.exports = function(router){
    //Get products for a subcategory
    router.get('/products/:products', ctrlProducts.products);
    //Get product
    router.get('/products/:products/:productid', ctrlProducts.productid);
    //Get products searched
    router.get('/search/:name',ctrlProducts.findProduct)
}